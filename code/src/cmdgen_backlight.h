/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Generic commands handling for backlight module
 *
 * Needs to be included after communication module
 * as it detects it and includes appropriate version.
 *
 * Defines macro CMDGEN_BACKLIGHT(uint8_t* buffer, unsigned char length)
 * @param buffer - pointer to buffer with received data
 * @param length - length of received data in buffer
 */
#ifndef CMDGEN_BACKLIGHT_H_
#define CMDGEN_BACKLIGHT_H_


#include "backlight.h"
#include "commands_generic.h"


// detect communication module and include correct version
#ifdef TWISO_H_
	#define CMDGEN_BACKLIGHT(buffer, length) \
		case CMDGEN_BACKLIGHT_GET_UNIT:\
			twiso_start_byte(backlight_getIntensityUnit());\
			break;\
		case CMDGEN_BACKLIGHT_SET_UNIT:\
			if((length) > 1){/* if enough data, perform action */\
				backlight_setIntensityUnit((buffer)[1]);\
			};\
			twiso_start_nodata();/* start receiving again */\
			break;\
		case CMDGEN_BACKLIGHT_GET_NAME:\
			twiso_start_byte(backlight_getIntensityName());\
			break;\
		case CMDGEN_BACKLIGHT_SET_NAME:\
			if((length) > 1){/* if enough data, perform action */\
				backlight_setIntensityName((buffer)[1]);\
			};\
			twiso_start_nodata();/* start receiving again */\
			break;
#else
	#error "CMDGEN_BACKLIGHT: No compatible communication module detected!"
#endif


#endif
