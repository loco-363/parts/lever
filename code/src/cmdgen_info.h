/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Generic commands handling infos
 *
 * Needs to be included after communication module
 * as it detects it and includes appropriate version.
 *
 * Defines macro CMDGEN_INFO
 * (no parameters)
 */
#ifndef CMDGEN_INFO_H_
#define CMDGEN_INFO_H_


#include <inttypes.h>

#include "commands_generic.h"
#include "system.h"


// detect communication module and include correct version
#ifdef TWISO_H_
	#define CMDGEN_INFO \
		case CMDGEN_VENDOR_ID:\
			twiso_start_word(INFO_VENDOR_ID);\
			break;\
		case CMDGEN_PRODUCT_ID:\
			twiso_start_word(INFO_PRODUCT_ID);\
			break;\
		case CMDGEN_VERSION_ID:\
			{\
				uint16_t version = INFO_VERSION_ID;\
				uint8_t ver[] = {(version >> 8) & 0xFF, version & 0xFF};\
				twiso_start(ver, 2);\
			}\
			break;\
		case CMDGEN_VENDOR_LENGTH:\
			twiso_start_byte(INFO_VENDOR_LENGTH);\
			break;\
		case CMDGEN_VENDOR:\
			twiso_start_string(INFO_VENDOR, INFO_VENDOR_LENGTH);\
			break;\
		case CMDGEN_PRODUCT_LENGTH:\
			twiso_start_byte(INFO_PRODUCT_LENGTH);\
			break;\
		case CMDGEN_PRODUCT:\
			twiso_start_string(INFO_PRODUCT, INFO_PRODUCT_LENGTH);\
			break;\
		case CMDGEN_VERSION_LENGTH:\
			twiso_start_byte(INFO_VERSION_LENGTH);\
			break;\
		case CMDGEN_VERSION:\
			twiso_start_string(INFO_VERSION, INFO_VERSION_LENGTH);\
			break;
#else
	#error "CMDGEN_INFO: No compatible communication module detected!"
#endif


#endif
