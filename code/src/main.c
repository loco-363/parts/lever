/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>

#include "twiso/src/twiso.h"

#include "backlight.h"
#include "bool.h"
#include "commands_generic.h"
#include "commands_specific.h"
#include "sensor.h"
#include "system.h"


// generic commands processing macros
#include "cmdgen_backlight.h"
#include "cmdgen_info.h"


int main(){
	// init backlight
	backlight_init();
	
	// init sensor
	sensor_init();
	
	// configure and start TWI
	twiso_init_basic(I2C_ADDRESS, I2C_GENERAL, TRUE);
	twiso_start_nodata();
	
	// global interrupt enable
	sei();
	
	// init local buffer
	uint8_t buffer[5];
	
	while(TRUE){
		// process received data
		if(!twiso_isBusy()){
			unsigned char len = twiso_getData(buffer, 5);
			if(len > 0){// we have received some data, handle them
				switch(buffer[0]){
					case CMD_GET_VALUE:
						twiso_start_byte(sensor_getValue());
						break;
					case CMD_GET_POSITIONS:
						twiso_start_byte(sensor_getAllValues());
						break;
					case CMD_SINGLE_FINISHED:
						twiso_start_byte(sensor_ready());
						break;
					case CMD_SINGLE_START:
						if(sensor_getMode() == SENSOR_MODE_SINGLE){
							sensor_setMode(SENSOR_MODE_SINGLE_ACTIVE);
						};
						twiso_start_nodata();
						break;
					case CMD_GET_MODE:
						twiso_start_byte(sensor_getMode());
						break;
					case CMD_MODE_SINGLE:
					case CMD_MODE_SINGLE_START:
					case CMD_MODE_CONTINUOUS:
						sensor_setMode((buffer[0] & 0x0F) - 1);
						twiso_start_nodata();
						break;
					case CMD_GET_FILTER:
						twiso_start_byte(sensor_getFilter() + 1);
						break;
					case CMD_FILTER_NONE:
					case CMD_FILTER_BACKGROUND:
					case CMD_FILTER_SEQUENCED_SIMPLE:
					case CMD_FILTER_SEQUENCED_SIMPLE_BACKGROUND:
					case CMD_FILTER_SEQUENCED:
					case CMD_FILTER_SEQUENCED_BACKGROUND:
						sensor_setFilter((buffer[0] & 0x0F) - 1);
						twiso_start_nodata();
						break;
					
					// backlight commands
					CMDGEN_BACKLIGHT(buffer, len)
					
					// info commands
					CMDGEN_INFO
					
					default:// nothing, we could handle; start TWI for receiving again
						twiso_start_nodata();
				};
			};
		};
	};
}
