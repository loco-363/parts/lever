/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "sensor.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "system.h"


/**
******************************
***** INTERNAL VARIABLES *****
******************************
*/

/** Internal flags */
volatile unsigned char sensor_flags = 0;

/** Flags bits */
#define SENSOR_FLAGS_FINISHED   (1 << 1)// acquisition finished


/** Modes */
volatile uint8_t sensor_mode = 0;
volatile uint8_t sensor_filter = 0;


/** Value read */
volatile uint8_t sensor_value = (1 << 7);// zero value with invalid flag set



/**
*********************
***** FUNCTIONS *****
*********************
*/

void sensor_init(){
	// ensure the interrupt is disabled
	ADCSRA = 0;
	
	// reset internal variables
	sensor_flags = 0;
	sensor_mode = 0;
	sensor_filter = 0;
	sensor_value = (1 << 7);// zero value with invalid flag set
	
	// set LED pins as outputs
	DDRC |= OPTO_LED_0;
	DDRB |= OPTO_LED_1 | OPTO_LED_2 | OPTO_LED_3 | OPTO_LED_4;
	
	// set fototransistor pins as inputs + enable internal pull-ups
	DDRA &= ~(OPTO_FOTO_0 | OPTO_FOTO_1 | OPTO_FOTO_2 | OPTO_FOTO_3 | OPTO_FOTO_4);
	PUEA |= OPTO_FOTO_0 | OPTO_FOTO_1 | OPTO_FOTO_2 | OPTO_FOTO_3 | OPTO_FOTO_4;
	//TODO we might disable the digital I/O function by DIDR0 + PORTCR registers, but could we then use the internal pull-ups?
	
	// setup ADC
	ADMUX = 0;// AREF=Vcc, select ADC channel 0
	ADCSRB = (1 << ADLAR);// ADC result left adjusted - 8-bit precision is perfectly OK for us
	ADCSRA = (1 << ADEN) // enable ADC
	       | (1 << ADIE) // enable Conversion Complete Interrupt
	       | (1 << ADPS2);// set prescaler to 0b100 = CK/16
	//       | (1 << ADPS2) | (1 << ADPS1);// set prescaler to 0b110 = CK/64
	
	// Note: For best precision, clock needs to be between 50kHz and 200kHz
	// 1MHz / 16 = 62.5kHz
	// 8MHz / 64 = 125kHz
	
	// let the setters configure the modes
	sensor_setFilter(0);
	sensor_setMode(0);
} // sensor_init

uint8_t sensor_getValue(){
	return sensor_value;
} // sensor_getValue

uint8_t sensor_getAllValues(){
	return OPTO_LAST - OPTO_FIRST + 1;
} // sensor_getAllValues

unsigned char sensor_ready(){
	return BOOL(sensor_flags & SENSOR_FLAGS_FINISHED);
} // sensor_ready

uint8_t sensor_getMode(){
	return sensor_mode;
} // sensor_getMode

void sensor_setMode(uint8_t mode){
	// check mode validity
	if(mode >= SENSOR_MODES || sensor_mode == mode){
		return;
	};
	
	// switch mode
	sensor_mode = mode;
	
	// detect if we should enable or disable the acquisition
	if(mode){// turn LEDs ON + start
		// reset the finished flag
		sensor_flags &= SENSOR_FLAGS_FINISHED;
		
		// LEDs
		PORTC |= OPTO_LED_0;
		PORTB |= OPTO_LED_1 | OPTO_LED_2 | OPTO_LED_3 | OPTO_LED_4;
		
		// select first channel in MUX
		ADMUX = (ADMUX & 0xF0) | OPTO_FIRST;
		
		// start ADC conversion
		ADCSRA |= (1 << ADSC);
	}
	else {// switch LEDs OFF - no need to terminate the acquisition, it will stop during next interrupt
		PORTC &= ~(OPTO_LED_0);
		PORTB &= ~(OPTO_LED_1 | OPTO_LED_2 | OPTO_LED_3 | OPTO_LED_4);
	};
} // sensor_setMode

uint8_t sensor_getFilter(){
	return sensor_filter;
} // sensor_getFilter

void sensor_setFilter(uint8_t mode){
	// check mode validity
	if(mode >= SENSOR_FILTERS || sensor_filter == mode){
		return;
	};
	
	//TODO no other filter than NONE implemented - no need to implement a change
} // sensor_setFilter



/**
******************************
***** Interrupt routines *****
******************************
*/

/**
 * ADC - Conversion Complete
 */
ISR(ADC_vect){
	// acquisition-internal variables (persistent accross ISRs)
	volatile static uint8_t guess = 0;
	volatile static uint8_t min = 255;
	
	// handle acquisition
	if(sensor_mode){// we should run
		// get channel of just finished conversion
		uint8_t channel = (ADMUX & 0x0F);
		
		// process measurement
		uint8_t meas = ADCH;
		if(meas < min){// the fototransistor is connected between ADC input and GND, so more light means lower voltage
			guess = channel;
			min = meas;
		};
		
		// select next channel
		channel++;
		if(channel > OPTO_LAST){// just finished the cycle
			// select the first channel again
			channel = OPTO_FIRST;
			
			// save result
			sensor_value = guess - OPTO_FIRST;
			sensor_flags |= SENSOR_FLAGS_FINISHED;
			
			// reset internal variables
			guess = 0;
			min = 255;
			
			// decide, what to do next
			if(sensor_mode != SENSOR_MODE_CONTINUOUS){// the finished cycle was a single one - stop
				sensor_mode = SENSOR_MODE_SINGLE;
				
				// switch LEDs OFF
				PORTC &= ~(OPTO_LED_0);
				PORTB &= ~(OPTO_LED_1 | OPTO_LED_2 | OPTO_LED_3 | OPTO_LED_4);
				
				return;
			};
		};
		
		// select channel in MUX
		ADMUX = (ADMUX & 0xF0) | channel;
		
		// start next conversion
		ADCSRA |= (1 << ADSC);
	}
	else {// we should be stopped - reset internal variables
		guess = 0;
		min = 255;
	};
} // ISR - ADC
