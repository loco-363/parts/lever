/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Sensor module itself
 */
#ifndef SENSOR_H_
#define SENSOR_H_


#include <inttypes.h>


/** Configuration */
// Modes
#define SENSOR_MODES   3
#define SENSOR_MODE_SINGLE          0
#define SENSOR_MODE_SINGLE_ACTIVE   1
#define SENSOR_MODE_CONTINUOUS      2
// Filter modes
#define SENSOR_FILTERS   6
#define SENSOR_FILTER_NONE                          0
#define SENSOR_FILTER_BACKGROUND                    1
#define SENSOR_FILTER_SEQUENCED_SIMPLE              2
#define SENSOR_FILTER_SEQUENCED_SIMPLE_BACKGROUND   3
#define SENSOR_FILTER_SEQUENCED                     4
#define SENSOR_FILTER_SEQUENCED_BACKGROUND          5


/**
 * Init sensor
 */
void sensor_init();

/**
 * Returns last acquisited value
 * @return value
 */
uint8_t sensor_getValue();

/**
 * Returns all possible values (positions/states of control input)
 */
uint8_t sensor_getAllValues();

/**
 * Returns whether the sensor is ready (acquisition finished)
 * Used in single mode only.
 * @return finished or not
 */
unsigned char sensor_ready();

/**
 * Returns actual mode of sensor
 * @return mode
 */
uint8_t sensor_getMode();

/**
 * Sets mode of sensor (also starts single mode)
 * @param mode
 */
void sensor_setMode(uint8_t mode);

/**
 * Returns actual mode of input filter
 * @return filter mode
 */
uint8_t sensor_getFilter();

/**
 * Sets mode of input filter
 * @param filter mode
 */
void sensor_setFilter(uint8_t mode);


#endif
