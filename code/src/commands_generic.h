/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * I2C Commands - generic
 */
#ifndef COMMANDS_GENERIC_H_
#define COMMANDS_GENERIC_H_


typedef enum {
	/** IDs */
	CMDGEN_VENDOR_ID = 0x01,
	CMDGEN_PRODUCT_ID = 0x02,
	CMDGEN_VERSION_ID = 0x03,
	
	/** ID strings */
	CMDGEN_VENDOR_LENGTH = 0x05,
	CMDGEN_VENDOR = 0x06,
	CMDGEN_PRODUCT_LENGTH = 0x07,
	CMDGEN_PRODUCT = 0x08,
	CMDGEN_VERSION_LENGTH = 0x09,
	CMDGEN_VERSION = 0x0A,
	
	/** Back-light */
	CMDGEN_BACKLIGHT_GET_UNIT = 0x10,
	CMDGEN_BACKLIGHT_SET_UNIT = 0x11,
	CMDGEN_BACKLIGHT_GET_NAME = 0x12,
	CMDGEN_BACKLIGHT_SET_NAME = 0x13,
} COMMAND_GENERIC_T;


#endif
