/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * I2C Commands - specific
 */
#ifndef COMMANDS_SPECIFIC_H_
#define COMMANDS_SPECIFIC_H_


typedef enum {
	/** *** Data *** */
	
	/**
	 * Returns (prepares for next READ) last read value of control input position
	 * @return byte - MSB indicates:
	 *   0 - the control input is directly in the indicated position
	 *   1 - the control input is somewhere between valid positions (the value returned is the last valid)
	 */
	CMD_GET_VALUE = 0x20,
	
	/**
	 * Returns (prepares for next READ) the number of all positions of the control input
	 * @return byte
	 */
	CMD_GET_POSITIONS = 0x21,
	
	/**
	 * Returns if the last triggered single mode is finished, hence the data valid and ready to be read out
	 * When in continuous mode, this will always read as zero
	 * @return bool/byte
	 */
	CMD_SINGLE_FINISHED = 0x2F,
	
	
	/** *** Commands *** */
	
	/**
	 * Starts reading if in single mode (will be ignored if in continuous mode)
	 */
	CMD_SINGLE_START = 0x30,
	
	
	/** *** Mode (of data acquisition) *** */
	
	/**
	 * Returns actual mode
	 * @return byte
	 *   - 0x00 = single mode, no acquisition in progress
	 *   - mode as 0x01 - 0x0F (the low-nibble only)
	 */
	CMD_GET_MODE = 0x40,
	
	/**
	 * Switch to single mode - read just once when requested
	 */
	CMD_MODE_SINGLE = 0x41,// default
	
	/**
	 * Switch to single mode and starts acquisition
	 */
	CMD_MODE_SINGLE_START = 0x42,
	
	/**
	 * Read continuously
	 * Will use the highest available speed of ADC
	 * with sufficient resolution and with respect to MCU load
	 * to not restrict other interrupts (especially the one used for communication)
	 */
	CMD_MODE_CONTINUOUS = 0x43,
	
	
	/** *** Input filering *** */
	
	/**
	 * Returns actual filter setting
	 * @return byte - setting as 0x01 - 0x0F (the low-nibble only)
	 */
	CMD_GET_FILTER = 0x50,
	
	/**
	 * Switch all LEDs ON and guess the actual position as the most lighted fototransistor
	 */
	CMD_FILTER_NONE = 0x51,// default
	
	/**
	 * Like NONE, but the acquisition cycle will consist of one reading with LEDs enabled and one with disabled
	 * Position will be then guessed as the most lighted fototransistor after subtracting
	 * the reading of background (with disabled LEDs)
	 */
	CMD_FILTER_BACKGROUND = 0x52,
	
	/**
	 * Performs readings while sequentially switching the LEDs during single acquisition cycle
	 * Will read only the fototransistor facing the lighted LED
	 * and the most lighted reading will be considered as actual value.
	 */
	CMD_FILTER_SEQUENCED_SIMPLE = 0x53,
	
	/**
	 * Like SEQUENCED_SIMPLE, but will also read the fototransistors far from the lighted LED
	 * to determine their background value. The position will be guesed the same way, just
	 * the background will be subtracted from the readings first before comparing them.
	 */
	CMD_FILTER_SEQUENCED_SIMPLE_BACKGROUND = 0x54,
	
	CMD_FILTER_SEQUENCED = 0x55,
	CMD_FILTER_SEQUENCED_BACKGROUND = 0x56
} COMMAND_SPECIFIC_T;


#endif
