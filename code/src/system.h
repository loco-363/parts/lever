/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * System constants & macros
 */
#ifndef SYSTEM_H_
#define SYSTEM_H_


#include <inttypes.h>

#include "bool.h"


/** ASM Macros */
#define _NOP()  __asm__ __volatile__("nop")
#define _WDR()  __asm__ __volatile__("wdr")

/** Splitting 16-bit word to bytes */
#define loByte(i) ((uint8_t) i)
#define hiByte(i) ((uint8_t) (((uint16_t) i) >> 8))


/** I2C */
/** 7-bit address */
#ifndef I2C_ADDRESS
	#define I2C_ADDRESS   0x20
#endif

/** General-call enable*/
#ifndef I2C_GENERAL
	#define I2C_GENERAL   FALSE
#endif


/** Sensor */
/** First position */
#ifndef OPTO_FIRST
	#define OPTO_FIRST   0
#endif

/** Last position */
#ifndef OPTO_LAST
	#define OPTO_LAST   4
#endif


/** Info */
/** Vendor - ID, string & length */
#define INFO_VENDOR_ID   0x0001
#define INFO_VENDOR   "Elektro-potkan"
#define INFO_VENDOR_LENGTH   14

/** Product - ID, string & length */
#define INFO_PRODUCT_ID   0x0001
#define INFO_PRODUCT   "Lever"
#define INFO_PRODUCT_LENGTH   5

/** Version - ID, string & length */
#define INFO_VERSION_ID   0x0102
#define INFO_VERSION   "v1.2"
#define INFO_VERSION_LENGTH   4


/** Hardware */
/** Optical barrier */
#define OPTO_FOTOS   5
#define OPTO_FOTO_0   (1 << PA0)
#define OPTO_FOTO_1   (1 << PA1)
#define OPTO_FOTO_2   (1 << PA2)
#define OPTO_FOTO_3   (1 << PA3)
#define OPTO_FOTO_4   (1 << PA4)
#define OPTO_LEDS   5
#define OPTO_LED_0   (1 << PC0)
#define OPTO_LED_1   (1 << PB3)
#define OPTO_LED_2   (1 << PB2)
#define OPTO_LED_3   (1 << PB1)
#define OPTO_LED_4   (1 << PB0)

/** Back-light */
#define BL_UNIT   (1 << PC2)
#define BL_NAME   (1 << PC5)


/** Check external options */
/** I2C */
#if I2C_ADDRESS < 0x00 || I2C_ADDRESS > 0x7F
	#error "I2C address has to be one-byte unsigned integer!"
#endif

/** Sensor */
#if OPTO_FIRST < 0
	#error "Position should be a non-negative number!"
#endif

#if OPTO_FIRST >= OPTO_LAST
	#error "At least 2 positions should be defined!"
#endif

#if OPTO_LAST >= OPTO_FOTOS
	#error "There cannot be more positions than sensing fototransistors!"
#endif

#endif
