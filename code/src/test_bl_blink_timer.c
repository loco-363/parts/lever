/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>

#include "system.h"


/**
 * Timer0 Overflow Interrupt
 */
ISR(TIM0_OVF_vect){
	// toggle pin states
	PINC = BL_UNIT | BL_NAME;
} // ISR(TIM0_OVF_vect)

int main(){
	// set pins as outputs
	DDRC = BL_UNIT | BL_NAME;
	PORTC = BL_UNIT;
	
	// setup Timer0
	TCCR0B = (1 << CS02) | (1 << CS00);// prescaller set to clkIO/1024
	TIMSK = (1 << TOIE0);// enable Timer0 Overflow Interrupt
	
	// global interrupt enable
	sei();
	
	while(TRUE);
}
