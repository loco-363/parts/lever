/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "backlight.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "bool.h"


// Test MCU compatiblity
#ifdef BACKLIGHT_PWM
	#if !defined(__AVR_ATtiny40__)
		#error "PWM backlight unsupported on current MCU!"
	#endif
#endif



/**
****************************
***** INTERNAL HELPERS *****
****************************
*/
#ifdef BACKLIGHT_PWM

/**
 * Enables Timer clock
 */
inline void backlight_timer_start(){
	TCCR1A |= (1 << CS11) | (1 << CS10);// set prescaler to 0b011 = clkIO/64
} // twiso_twi_enable

/**
 * Disables Timer clock
 */
inline void backlight_timer_stop(){
	TCCR1A &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));// set prescaler to 0b000 = No clock source
} // twiso_twi_disable

#endif



/**
*********************
***** FUNCTIONS *****
*********************
*/

void backlight_init(){
	// set as output
	BACKLIGHT_NAME_DDR |= (1 << BACKLIGHT_NAME_BIT);
	BACKLIGHT_UNIT_DDR |= (1 << BACKLIGHT_UNIT_BIT);
	
	#ifdef BACKLIGHT_PWM
		// set-up Timer and its Interrupts
		TIMSK &= ~(1 << ICIE1);// explicitly disable Input Capture Interrupt
		TIMSK |= (1 << TOIE1);// enable Overflow Interrupt - Compare Match A/B Interrupts are fully managed by setters
		// no need to enable the clock now - it is also managed by setters
	#endif
	
	// set zero intensities - e.g. both backlights OFF
	backlight_setIntensityName(FALSE);
	backlight_setIntensityUnit(FALSE);
} // backlight_init

uint8_t backlight_getIntensityName(){
	#ifdef BACKLIGHT_PWM
		return OCR1A;
	#else
		return BOOL(BACKLIGHT_NAME_PIN & (1 << BACKLIGHT_NAME_BIT));
	#endif
} // backlight_getIntensityName

void backlight_setIntensityName(uint8_t intensity){
	#ifdef BACKLIGHT_PWM
		OCR1A = intensity;
		
		// manage interrupts to save MCU cycles
		if(intensity > 0 && intensity < 255){// regulated intensity, so we need the interrupts
			TIMSK |= (1 << OCIE1A);
			backlight_timer_start();
		}
		else {// full-power or off - no interrupts needed
			TIMSK &= ~(1 << OCIE1A);
			
			if(!(TIMSK & (1 << OCIE1B))){// the other Compare Match Interrupt is already disabled - no interrupt needed at all - we could safely stop the Timer
				backlight_timer_stop();
			};
			
			// as we disabled the interrupt, we have to ensure the backlight will be turned ON or OFF as requested
			if(intensity){
				BACKLIGHT_NAME_PORT |= (1 << BACKLIGHT_NAME_BIT);
			}
			else {
				BACKLIGHT_NAME_PORT &= ~(1 << BACKLIGHT_NAME_BIT);
			};
		};
	#else
		if(intensity){
			BACKLIGHT_NAME_PORT |= (1 << BACKLIGHT_NAME_BIT);
		}
		else {
			BACKLIGHT_NAME_PORT &= ~(1 << BACKLIGHT_NAME_BIT);
		};
	#endif
} // backlight_setIntensityName

uint8_t backlight_getIntensityUnit(){
	#ifdef BACKLIGHT_PWM
		return OCR1B;
	#else
		return BOOL(BACKLIGHT_UNIT_PIN & (1 << BACKLIGHT_UNIT_BIT));
	#endif
} // backlight_getIntensityUnit

void backlight_setIntensityUnit(uint8_t intensity){
	#ifdef BACKLIGHT_PWM
		OCR1B = intensity;
		
		// manage interrupts to save MCU cycles
		if(intensity > 0 && intensity < 255){// regulated intensity, so we need the interrupts
			TIMSK |= (1 << OCIE1B);
			backlight_timer_start();
		}
		else {// full-power or off - no interrupts needed
			TIMSK &= ~(1 << OCIE1B);
			
			if(!(TIMSK & (1 << OCIE1A))){// the other Compare Match Interrupt is already disabled - no interrupt needed at all - we could safely stop the Timer
				backlight_timer_stop();
			};
			
			// as we disabled the interrupt, we have to ensure the backlight will be turned ON or OFF as requested
			if(intensity){
				BACKLIGHT_UNIT_PORT |= (1 << BACKLIGHT_UNIT_BIT);
			}
			else {
				BACKLIGHT_UNIT_PORT &= ~(1 << BACKLIGHT_UNIT_BIT);
			};
		};
	#else
		if(intensity){
			BACKLIGHT_UNIT_PORT |= (1 << BACKLIGHT_UNIT_BIT);
		}
		else {
			BACKLIGHT_UNIT_PORT &= ~(1 << BACKLIGHT_UNIT_BIT);
		};
	#endif
} // backlight_setIntensityUnit



/**
******************************
***** Interrupt routines *****
******************************
*/
#ifdef BACKLIGHT_PWM

/**
 * Overflow - both ON
 */
ISR(TIM1_OVF_vect){
	if(OCR1A > 0){// leave OFF for 0 intensity - otherwise the LED still glows
		BACKLIGHT_NAME_PORT |= (1 << BACKLIGHT_NAME_BIT);
	};
	
	if(OCR1B > 0){// leave OFF for 0 intensity - otherwise the LED still glows
		BACKLIGHT_UNIT_PORT |= (1 << BACKLIGHT_UNIT_BIT);
	};
} // ISR - Overflow

/**
 * Compare Match A - Name OFF
 */
ISR(TIM1_COMPA_vect){
	BACKLIGHT_NAME_PORT &= ~(1 << BACKLIGHT_NAME_BIT);
} // ISR - Compare Match A

/**
 * Compare Match B - Unit OFF
 */
ISR(TIM1_COMPB_vect){
	BACKLIGHT_UNIT_PORT &= ~(1 << BACKLIGHT_UNIT_BIT);
} // ISR - Compare Match B

#endif
