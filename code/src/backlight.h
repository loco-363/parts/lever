/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Backlight-handling module
 */
#ifndef BACKLIGHT_H_
#define BACKLIGHT_H_


#include <avr/io.h>
#include <inttypes.h>


/** Type of backlight - use PWM if defined, otherwise simple ON/OFF */
#define BACKLIGHT_PWM

/** Hardware configuration */
// Name
#define BACKLIGHT_NAME_DDR    DDRC
#define BACKLIGHT_NAME_PIN    PINC
#define BACKLIGHT_NAME_PORT   PORTC
#define BACKLIGHT_NAME_BIT   PC5
// Unit
#define BACKLIGHT_UNIT_DDR    DDRC
#define BACKLIGHT_UNIT_PIN    PINC
#define BACKLIGHT_UNIT_PORT   PORTC
#define BACKLIGHT_UNIT_BIT   PC2


/**
 * Init backlight
 */
void backlight_init();

/**
 * Returns Name backlight intensity
 * @return PWM intensity (or TRUE/FALSE for simple ON/OFF)
 */
uint8_t backlight_getIntensityName();

/**
 * Sets Name backlight intensity
 * @param intensity - intensity for PWM or boolean for simple ON/OFF
 */
void backlight_setIntensityName(uint8_t intensity);

/**
 * Returns Unit backlight intensity
 * @return PWM intensity (or TRUE/FALSE for simple ON/OFF)
 */
uint8_t backlight_getIntensityUnit();

/**
 * Sets Unit backlight intensity
 * @param intensity - intensity for PWM or boolean for simple ON/OFF
 */
void backlight_setIntensityUnit(uint8_t intensity);


#endif
