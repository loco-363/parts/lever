/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <avr/io.h>
#include <inttypes.h>

#include "system.h"


int main(){
	// set pins as outputs
	DDRC = BL_UNIT | BL_NAME;
	PORTC = BL_UNIT;
	
	while(TRUE){
		// toggle pin states
		PINC = BL_UNIT | BL_NAME;
		
		// delay
		for(uint8_t i = 0; i < 255; i++){
			for(uint8_t j = 0; j < 255; j++);
		};
	};
}
