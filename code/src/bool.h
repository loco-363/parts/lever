/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Simple booleans and associated macros
 */
#ifndef BOOL_H_
#define BOOL_H_


/** Values */
#define FALSE 0
#define TRUE 1

/** Conversion macro(s) */
#define BOOL(var) ((var) ? TRUE : FALSE)
//#define BOOL(var) (!(!(var)))


#endif
