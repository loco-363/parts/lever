/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <generic/ir.scad>;
use <generic/nut_trap.scad>;
use <generic/threads.scad>;


/**
 * Lever housing
 * @param pos - definition of lever positions to generate
 *   The lever assembly is prepared for 5 positions, the pos vector is always counted starting from position "-2".
 *   To discard a position (e.g. if you want 3-position only assembly, you need to discard the first position), set the corresponding item to negative number (e.g. -1).
 *   Non-negative number enables the position (e.g. >= 0).
 * @param locksThickness - thickness of latches protrusions
 * @param mountH - correction of height of mountings
 *   Allows changing of the height, the lever housing would have, over the base panel surface.
 *   Could be adjusted to different base panel thickness. etc.
 * @param pcbMountHolesDiameter - diameter of holes for PCB mounting (assumed self-tapping screws)
 */
module housing(pos=[1, 1, 1, 1, 1], locksThickness=2, mountH=0, pcbMountHolesDiameter=2){
	difference(){
		union(){
			// base
			cylinder(r=40, h=15);
			
			// mountings + height positioning
			translate([-mountH-10, 0, 0]){
				difference(){
					translate([0, -(40+15), 0])
						cube([10, (40+15)*2, 15-0.5]);
					
					for(i = [-1 : 2 : 1]){
						translate([-1, i*(40+6), 15/2-1.5]){
							rotate([0, 90, 0]){
								cylinder(d=4.5, h=1+10+1);
								
								rotate([0, 0, 90])
									nut_trap(w=7.3, h=3.5+1);// nut w=6.9, h=3.2
							}
						}
					}
				}
			}
			
			// height positioning - additional material to mountings
			if(mountH != 0)
				translate([((mountH > 0) ? -mountH-10 : 0), -40, 0])
					cube([abs(mountH)+((mountH > 0) ? 10 : 0), 40*2, 15-0.5]);
			
			// latches - protrusions
			translate([0, 0, 15-1.5]){
				difference(){
					cylinder(r=40, h=1+2);
					
					translate([0, 0, -1]){
						cylinder(r=40-locksThickness, h=1+(1+2)+1);
						
						for(i = [-1 : 2 : 1])
							rotate([0, 0, -45+i*((180-120)-45)])
								cube([40+1, 40+1, 1+(1+2)+1]);
					}
				}
			}
		}
		
		// latches - cut-outs
		translate([0, 0, 15-2.5]){
			intersection(){
				difference(){
					cylinder(r=40+0.5, h=2+1+2);
					
					translate([0, 0, -1])
						cylinder(r=40-locksThickness-0.5, h=1+(2+1+2)+1);
				}
				
				translate([0, 0, -1]){
					for(i = [-1 : 1]){
						rotate([0, 0, 90+i*(120/3)*2+1]){
							intersection(){
								cube([40+1, 40+1, 1+(2+1+2)+1]);
								
								rotate([0, 0, 90-120/3-2])
									cube([40+1, 40+1, 1+(2+1+2)+1]);
							}
						}
					}
				}
			}
		}
		
		// housing screwing holes and nut traps
		translate([0, 0, -1]){
			for(j = [-1 : 2 : 1]){
				for(i = [1 : 3]){
					rotate([0, 0, j*(i-1+1/2)*(2*120/5)]){
						translate([-33, 0, 0]){
							difference(){
								cylinder(d=3.5, h=1+15+1);
								
								translate([0, 0, 3+1])
									cylinder(d=3.5+1, h=0.3);// bridge for closing bolt/nut space
							}
							
							if(j > 0){
								cylinder(d=6.5, h=3+1);// bolt head d=5.9
							}
							else {
								rotate([0, 0, 90])
									nut_trap(w=5.8, h=3+1);// nut w=5.4
							}
						}
					}
				}
			}
		}
		
		// axle
		translate([0, 0, -1])
			cylinder(r=6+0.5, h=1+15+1);
		
		// wheel cut-out
		translate([0, 0, 15-(4+0.5)])
			cylinder(r=25+2, h=(4+0.5)+1);
		
		// handle cut-out
		translate([0, 0, 15-4]){
			// cut height
			ch = (40+15)*sqrt(2) + abs(mountH) + 1;
			// cut width
			cw = 8;
			
			intersection(){
				// base cut-out
				union(){
					rotate([0, 0, -45])
						cube([ch, ch, cw]);
					
					for(i = [-1 : 2 : 1])
						rotate([0, 0, i*45])
							translate([0, 0, cw/2])
								rotate([0, 90, 0])
									cylinder(d=cw, h=ch);
				}
				
				// range reduction - from beginning
				if(len(pos) >= 1 && pos[0] < 0){
					rotate([0, 0, -45+22.5])
						cube([ch, ch, cw]);
					
					rotate([0, 0, -22.5])
						translate([0, 0, cw/2])
							rotate([0, 90, 0])
								cylinder(d=cw, h=ch);
				}
				
				if(len(pos) >= 2 && pos[0] < 0 && pos[1] < 0){
					rotate([0, 0, -45+2*22.5])
						cube([ch, ch, cw]);
					
					translate([0, 0, cw/2])
						rotate([0, 90, 0])
							cylinder(d=cw, h=ch);
				}
				
				if(len(pos) >= 3 && pos[0] < 0 && pos[1] < 0 && pos[2] < 0){
					rotate([0, 0, -45+3*22.5])
						cube([ch, ch, cw]);
					
					rotate([0, 0, 22.5])
						translate([0, 0, cw/2])
							rotate([0, 90, 0])
								cylinder(d=cw, h=ch);
				}
				
				// range reduction - from end
				if(len(pos) >= 5 && pos[4] < 0){
					rotate([0, 0, -45-22.5])
						cube([ch, ch, cw]);
					
					rotate([0, 0, 22.5])
						translate([0, 0, cw/2])
							rotate([0, 90, 0])
								cylinder(d=cw, h=ch);
				}
				
				if(len(pos) >= 5 && pos[4] < 0 && pos[3] < 0){
					rotate([0, 0, -45-2*22.5])
						cube([ch, ch, cw]);
					
					translate([0, 0, cw/2])
						rotate([0, 90, 0])
							cylinder(d=cw, h=ch);
				}
				
				if(len(pos) >= 5 && pos[4] < 0 && pos[3] < 0 && pos[2] < 0){
					rotate([0, 0, -45-3*22.5])
						cube([ch, ch, cw]);
					
					rotate([0, 0, -22.5])
						translate([0, 0, cw/2])
							rotate([0, 90, 0])
								cylinder(d=cw, h=ch);
				}
			}
		}
		
		// cut-out for returning springs
		translate([0, 0, 15-(4+0.5)-5]){
			intersection(){
				translate([0, -(10+2), 0])
					cube([10+2, 2*(10+2), 5+1]);
				
				cylinder(r=10+0.8, 5+1);
			}
		}
		
		// 5mm opto-barrier parts (LEDS / phototransistors)
		for(i = [-2 : 2]){
			rotate([0, 0, i*22.5]){
				translate([-((15+2)+(20-(15+2))/2), 0, 0]){
					if(len(pos) >= i+2 && pos[i+2] > 0){
						rotate([0, 0, 180-i*22.5])
							ir_5mm();
						cylinder(d=2.8, h=15+1);
					}
				}
			}
		}
		
		// opto-barrier shield cut-out
		translate([0, 0, 15-(4+0.5)-3]){
			intersection(){
				difference(){
					cylinder(r=(15+2-1)+0.5, h=3+1);
					
					translate([0, 0, -1])
						cylinder(r=15-0.5, h=1+(3+1)+1);
				}
				
				for(i = [-1 : 1])
				rotate([0, 0, 180+i*(45+5)])
					translate([0, -(20+1), 0])
						cube([20+1, (20+1)*2, 1+(3+1)+1]);
			}
		}
		
		// positions holding
		translate([17.5, 0, -1])
			metric_thread(diameter=8, length=1+15+1, internal=true);
		
		// PCB mounting
		for(i = [-2 : 2])
			rotate([0, 0, i*45])
				translate([-33, 0, -1])
					cylinder(d=pcbMountHolesDiameter, h=1+15+1);
	}
} // housing


housing();
