/**
***********************************************
***         Loco363 - Parts - Lever         ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


use <generic/nut_trap.scad>;


/**
 * Lever wheel
 * @param pos - definition of lever positions to generate
 *   The lever assembly is preapred for 5 positions, the pos vector is always counted starting from position "-2".
 *   To discard a position (e.g. if you want 3-position only assembly, you need to discard the first position), set the corresponding item to negative number (e.g. -1).
 *   Non-negative number enables the position (e.g. >= 0).
 *     - if zero, than the position is not marked (no cut-out for holder plunger)
 *     - if one, the position is marked, but not holded (shallow cut-out for plunger)
 *     - if two (or simply grater than one as of now), the positions is marked and will be holded (deep cut-out for plunger)
 */
module wheel(pos=[1, 1, 1, 1, 1]){
	/**
	 * Generates track for holder plunger according to selected positions
	 * @param positions - the same as top-level pos param
	 */
	module track(positions=[]){
		translate([0, 0, 4-1]){
			intersection(){
				difference(){
					cylinder(r=20, h=1+1);
					
					translate([0, 0, -1])
						cylinder(r=15, h=1+(1+1)+1);
				}
				
				// basic cutout for positions
				union(){
					rotate([0, 0, -45])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					for(i = [-1 : 2 : 1])
						rotate([0, 0, i*45])
							translate([17.5, 0, 0])
								cylinder(d=5, h=1+(1+1)+1);
				}
				
				// range reduction - from beginning
				if(len(positions) >= 1 && positions[0] < 0){
					rotate([0, 0, -45-22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					rotate([0, 0, 22.5])
						translate([17.5, 0, 0])
							cylinder(d=5, h=1+(1+1)+1);
				}
				
				if(len(positions) >= 2 && positions[0] < 0 && positions[1] < 0){
					rotate([0, 0, -45-2*22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					translate([17.5, 0, 0])
						cylinder(d=5, h=1+(1+1)+1);
				}
				
				if(len(positions) >= 3 && positions[0] < 0 && positions[1] < 0 && positions[2] < 0){
					rotate([0, 0, -45-3*22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					rotate([0, 0, -22.5])
						translate([17.5, 0, 0])
							cylinder(d=5, h=1+(1+1)+1);
				}
				
				// reduce range - from end
				if(len(positions) >= 5 && positions[4] < 0){
					rotate([0, 0, -45+22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					rotate([0, 0, -22.5])
						translate([17.5, 0, 0])
							cylinder(d=5, h=1+(1+1)+1);
				}
				
				if(len(positions) >= 5 && positions[4] < 0 && positions[3] < 0){
					rotate([0, 0, -45+2*22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					translate([17.5, 0, 0])
						cylinder(d=5, h=1+(1+1)+1);
				}
				
				if(len(positions) >= 5 && positions[4] < 0 && positions[3] < 0 && positions[2] < 0){
					rotate([0, 0, -45+3*22.5])
						cube([20+1, 20+1, 1+(1+1)+1]);
					
					rotate([0, 0, 22.5])
						translate([17.5, 0, 0])
							cylinder(d=5, h=1+(1+1)+1);
				}
			}
			
			// positions holding
			for(i = [-2 : 2])
				if(len(positions) >= i+2 && positions[i+2] > 0)
					rotate([0, 0, -i*22.5])
						translate([17.5, 0, 2.5-0.5*((len(positions) >= i+2 && positions[i+2] > 1) ? 2 : 1)])
							sphere(d=5);
		}
	} // track
	
	
	// MAIN part
	difference(){
		union(){
			// base
			translate([0, 0, -4])
				cylinder(r=25, h=8);
			
			// axle
			translate([0, 0, -(5+10)])
				cylinder(r=6, h=(5+10)*2);
			
			// protrusion for returning springs
			translate([0, 0, -(4+5)]){
				intersection(){
					translate([0, -3, 0])
						cube([10, 6, (4+5)*2]);
					
					cylinder(r=10, (4+5)*2);
				}
			}
			
			// opto-barrier shield
			translate([0, 0, -4-3]){
				intersection(){
					difference(){
						cylinder(r=15+2-1, h=3+8+3);
						
						translate([0, 0, -1])
							cylinder(r=15, h=1+(3+8+3)+1);
					}
					
					rotate([0, 0, 180])
						translate([0, -(20+1), 0])
							cube([20+1, (20+1)*2, 1+(3+8+3)+1]);
				}
			}
		}
		
		// axle screw
		translate([-1, 0, 0]){
			translate([0, 0, -(5+10)-1])
				cylinder(d=3.5, h=(5+10+1)*2);
			
			translate([0, 0, (5+10)-3])
				cylinder(d=6.5, h=3+1);// bolt head d=5.9
			
			rotate([180, 0, 90])
				translate([0, 0, (5+10)-3])
					nut_trap(w=5.8, h=3+1);// nut w=5.4
		}
		
		// handle
		translate([25-5, -5, -3])
			cube([5+1, 10, 6]);
		
		// handle screw
		rotate([0, 90, 0]){
			translate([0, 0, -10])
				cylinder(d=4.5, h=10+25+1);
			
			translate([0, 0, 3])
				nut_trap(w=7.5, h=5);// nut w=6.9
		}
		
		// opto-barrier window
		translate([0, 0, -4-1]){
			intersection(){
				difference(){
					cylinder(r=20, h=1+8+1);
					
					translate([0, 0, -1])
						cylinder(r=15+2, h=1+(1+8+1)+1);
				}
				
				rotate([0, 0, (180-45)-(45-8)])
					cube([20+1, 20+1, 1+(1+8+1)+1]);
				rotate([0, 0, (180-45)+(45-8)])
					cube([20+1, 20+1, 1+(1+8+1)+1]);
				
			}
			
			for(i = [-1 : 2 : 1])
				rotate([0, 0, 180+i*8])
					translate([(15+2)+(20-(15+2))/2, 0, 0])
						cylinder(d=(20-(15+2)), h=1+8+1);
		}
		
		// tracks + positions marking
		track(pos);
		
		mirror([0, 0, 1])
			track(pos);
	}
} // wheel


wheel();
