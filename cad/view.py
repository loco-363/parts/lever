# ***********************************************
# ***         Loco363 - Parts - Lever         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Lever(generic.bases.View):
	def __init__(self, c = None, bpos = -2, pos = range(-2, 3), dpos = 0, size = (40, 80), pos_left = False):
		'''Constructor
		
		@param c - center Coords
		@param bpos - base position
		@param pos - positions' labels
		@param dpos - the default (idle) position (the one, which the lever will return to)
		@param size - size of part's outline
		@param pos_left - draw positions' labels to the left from the track
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# check arguments
		bpos = int(bpos)
		if bpos < -2 or bpos > 2:
			raise ValueError('Base position must be an integer from -2 to +2!')
		
		if len(pos) < 1 or len(pos) > 5:
			raise ValueError('There could be from 1 to 5 positions!')
		
		if bpos + (len(pos)-1) > 2:
			raise ValueError('The uppermost position is +2!')
		
		# save positions parameter
		self._lpos_x = -10 if pos_left else 10
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			size[0] - 0.8,
			size[1] - 0.8,
			center=True,
			roundness=1,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': '0.8mm'
			}
		))
		
		# lever track
		if len(pos) > 1:
			self.add(gen_draw.shapes.Rectangle(
				C(c, (-4, -3.8+bpos*10-0.2)),
				8,
				10*(len(pos)-1)+7.6 + 0.4,
				roundness=4,
				properties={
					'fill': 'none',
					'stroke': 'black',
					'stroke-width': '0.4mm'
				}
			))
		
		# positions
		for i in range(len(pos)):
			if pos[i] != None:
				n = bpos + i
				self._addPosition(n, pos[i], n != dpos)
	# constructor
	
	def _addPosition(self, n, label, alt = False):
		'''Adds lever position
		
		@param n - position number
		@param label - position label
		@param alt - if True, draw the position as alternative (dashed)
		'''
		
		c = self.getCoords()
		
		# label
		self.add(gen_draw.shapes.Text(
			C(c, (self._lpos_x, n*10-2)),
			label,
			properties={
				'text-anchor': 'middle',
				'fill': 'black',
				'style': 'font-size:16pt; font-weight:bold;'
			}
		))
		
		# semi-transparent position fill
		self.add(gen_draw.shapes.Circle(
			C(c, (0, n*10)),
			3.8,
			properties={
				'fill': 'black',
				'opacity': 0.3 if alt else 0.5
			}
		))
		
		# position
		props = {
			'fill': 'none',
			'stroke': 'black',
			'stroke-width': '0.2mm' if alt else '0.3mm'
		}
		
		if alt:
			props['stroke-dasharray'] = '1,1'
		
		self.add(gen_draw.shapes.Circle(
			C(c, (0, n*10)),
			3.8 - (0.1 if alt else 0.15),
			properties=props
		))
	# _addPosition
# class Lever


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Lever(), True))
