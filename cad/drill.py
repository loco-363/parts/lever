# ***********************************************
# ***         Loco363 - Parts - Lever         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Lever(generic.bases.Drill):
	def __init__(self, *args, cutout = (36, 80), **kwargs):
		'''Constructor
		
		@param cutout - size of cutout in panel for lever body
		'''
		
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# coords
		self.add(generic.CoordsAuto(c, pos=(0, -5, 'middle')))
		
		# mounting holes
		self.add(generic.DrillHole(C(c, (-15/2-1.5, -(40+6))), 4, 'Z', pos=(-3, -2,   'end'), cpos=( 15/2+1, -6, 'end')))
		self.add(generic.DrillHole(C(c, ( 15/2+1.5, -(40+6))), 4, 'Z', pos=( 3, -2, 'start'), cpos=(-15/2-1, -6, 'start')))
		self.add(generic.DrillHole(C(c, (-15/2-1.5,  (40+6))), 4, 'Z', pos=(-3, -2,   'end'), cpos=( 15/2+1,  3, 'end')))
		self.add(generic.DrillHole(C(c, ( 15/2+1.5,  (40+6))), 4, 'Z', pos=( 3, -2, 'start'), cpos=(-15/2-1,  3, 'start')))
		
		# panel cut-out
		self.add(generic.LabeledPolygon(
			c,
			[
				(-cutout[0]/2, -cutout[1]/2, ( cutout[0]/2-0.5,  3, 'end')),
				( cutout[0]/2, -cutout[1]/2, (-cutout[0]/2+0.5,  3, 'start')),
				( cutout[0]/2,  cutout[1]/2, (-cutout[0]/2+0.5, -6, 'start')),
				(-cutout[0]/2,  cutout[1]/2, ( cutout[0]/2-0.5, -6, 'end'))
			],
			{
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 1.5
			}
		))
	# constructor
# class Lever


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Lever(), True))

