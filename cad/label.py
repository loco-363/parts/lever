# ***********************************************
# ***         Loco363 - Parts - Lever         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Lever(generic.bases.LabelPart):
	def __init__(self, *args, size = (40, 80), **kwargs):
		'''Constructor
		
		@param size - size of part's outline
		'''
		
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			size[0],
			size[1],
			center=True,
			properties={
				'fill': 'none',
				'stroke': self.color,
				'stroke-width': 3
			}
		))
	# constructor
# class Lever


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Lever(name='CL'), True))

